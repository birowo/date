package main

import (
	"errors"
)

const (
	DD   int = iota // 1 - 30
	MM              // 1 - 12
	MMM             // JAN - DES
	MMMM            // JANUARI - DESEMBER
	YY              // 21
	YYYY            // 2021
)

func IsLeapYear(year uint) bool {
	return year%4 == 0 && year%100 != 0 || year%400 == 0
}
func Atoui(str string) (res uint, err error) {
	for i := 0; i < len(str); i++ {
		if str[i] < '0' || str[i] > '9' {
			err = errors.New("err")
			return
		}
		res *= 10
		res += uint(str[i] - '0')
	}
	return
}
func IsValid(part1 int, sep1 string, part2 int, sep2 string, part3 int, date string) bool {
	dateLen := len(date)
	i := 0
	sep1Len := len(sep1)
	n := dateLen - sep1Len
	for i < n && date[i:i+sep1Len] != sep1 {
		i++
	}
	pos1 := i
	i += sep1Len
	sep2Len := len(sep2)
	n = dateLen - sep2Len
	for i < n && date[i:i+sep2Len] != sep2 {
		i++
	}
	if (i + sep2Len) >= dateLen {
		return false
	}
	parts := [3]string{date[:pos1], date[pos1+sep1Len : i], date[i+sep2Len:]}
	println(parts[0], ",", date[pos1:pos1+sep1Len], ",", parts[1], ",", date[i:i+sep2Len], ",", parts[2])
	var d, m, y, _d uint
	var err error
	for i, partFmt := range [3]int{part1, part2, part3} {
		switch partFmt {
		case DD:
			d, err = Atoui(parts[i])
			if err != nil || d < 1 {
				return false
			}
		case MM:
			m, err = Atoui(parts[i])
			if err != nil {
				return false
			}
			switch m {
			case 1:
				_d = 31
			case 2:
				_d = 29
			case 3:
				_d = 31
			case 4:
				_d = 30
			case 5:
				_d = 31
			case 6:
				_d = 30
			case 7:
				_d = 31
			case 8:
				_d = 31
			case 9:
				_d = 30
			case 10:
				_d = 31
			case 11:
				_d = 30
			case 12:
				_d = 31
			default:
				return false
			}
		case MMM:
			switch parts[i] {
			case "JAN":
				m, _d = 1, 31
			case "FEB":
				m, _d = 2, 29
			case "MAR":
				m, _d = 3, 31
			case "APR":
				m, _d = 4, 30
			case "MEI":
				m, _d = 5, 31
			case "JUN":
				m, _d = 6, 30
			case "JUL":
				m, _d = 7, 31
			case "AGU":
				m, _d = 8, 31
			case "SEP":
				m, _d = 9, 30
			case "OKT":
				m, _d = 10, 31
			case "NOV":
				m, _d = 11, 30
			case "DES":
				m, _d = 12, 31
			default:
				return false
			}
		case MMMM:
			switch parts[i] {
			case "JANUARI":
				m, _d = 1, 31
			case "FEBRUARI":
				m, _d = 2, 29
			case "MARET":
				m, _d = 3, 31
			case "APRIL":
				m, _d = 4, 30
			case "MEI":
				m, _d = 5, 31
			case "JUNI":
				m, _d = 6, 30
			case "JULI":
				m, _d = 7, 31
			case "AGUSTUS":
				m, _d = 8, 31
			case "SEPTEMBER":
				m, _d = 9, 30
			case "OKTOBER":
				m, _d = 10, 31
			case "NOVEMBER":
				m, _d = 11, 30
			case "DESEMBER":
				m, _d = 12, 31
			default:
				return false
			}
		case YY:
			y, err = Atoui(parts[i])
			if err != nil {
				return false
			}
			y += 2000
		case YYYY:
			y, err = Atoui(parts[i])
			if err != nil {
				return false
			}
		default:
			return false
		}
	}
	if d > _d || (d == 29 && m == 2 && !IsLeapYear(y)) {
		return false
	}
	return true
}

func main() {
	type f struct {
		p1 int
		s1 string
		p2 int
		s2 string
		p3 int
	}
	tbl := []struct {
		fmt f
		val string
	}{
		{f{DD, " - ", MMM, " - ", YYYY}, "21 - DES - 2021"},
		{f{DD, " ", MMM, " '", YY}, "21 DES '21"},
		{f{MMM, ", ", DD, " ", YYYY}, "DES, 21 2021"},
		{f{MMMM, ", ", DD, " ", YYYY}, "DESEMBER, 21 2021"},
		{f{DD, " / ", MM, " / ", YYYY}, "21 / 12 / 2021"},
		{f{YYYY, "-", MM, "-", DD}, "2021-12-21"},
		{f{YYYY, "-", MM, "-", DD}, "2000-2-29"}, //true : leap year
		{f{YYYY, "-", MM, "-", DD}, "2021-2-29"}, //false : not leap year
	}
	for _, itm := range tbl {
		println(IsValid(itm.fmt.p1, itm.fmt.s1, itm.fmt.p2, itm.fmt.s2, itm.fmt.p3, itm.val))
	}
}
